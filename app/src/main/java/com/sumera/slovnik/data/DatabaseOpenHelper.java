package com.sumera.slovnik.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.sumera.slovnik.R;
import com.sumera.slovnik.activity.MainActivity;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
	
	public static final String TAG = "DatabaseOpenHelper";
	
	private SQLiteDatabase mDatabase;

	public static final String DATABASE_NAME  = "main_database";
	private static final int DATABASE_VERSION = 1;
	
	/**
	 *  DICTIONARY TABLE
	 */
	public static final String TABLE_FOR_DICT   = "dict_table";
	public static final String COLUMN_WORD = "word";	
	public static final String COLUMN_DEF  = "def";
	public static final int    AMOUNT_OF_DEFS = 5;

	private final Context context;
	
	public boolean ready;
	
	/** Constructor */
	public DatabaseOpenHelper(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
		context = ctx;
		ready = ifDatabaseExist();
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		mDatabase = db;
        mDatabase.execSQL(getCreateTableSql());
        
        Log.d(TAG,"Create databases");
        
        Log.d(TAG,getCreateTableSql());
        
        loadDictionary();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOR_DICT );
		onCreate(db);
	}
	
	private String getCreateTableSql(){
		String create = "CREATE VIRTUAL TABLE " + TABLE_FOR_DICT +
						" USING fts3(tokenize=porter," + COLUMN_WORD +",";
		
		for(int i=0;i<AMOUNT_OF_DEFS;i++){
			create += COLUMN_DEF + String.valueOf(i+1);
			if(i+1 != AMOUNT_OF_DEFS){
				create += ",";
			}
		}
		create += ")";
		return create;		
	}
	
	private void loadDictionary() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    loadWordsFromFileSource();
                    ready = true;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();
    }

	private void loadWordsFromFileSource() throws IOException {
	    final Resources resources = context.getResources();
	    InputStream inputStream = resources.openRawResource(R.raw.all_words);
	    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	    try {
	        String   line;
	        String   word = "";
	        String[] defs = new String[AMOUNT_OF_DEFS];
	        Boolean  init  = true;	        
	        int indexOfDef = 0;
	        
	        while ((line = reader.readLine()) != null) {	        	
	        	if(!line.substring(0, 1).equals("#")){
	        		if(!init){
	        			addWordToDict(word,defs);
	        		}
	        		else {
	        			init = false;        			
	        		}
	        		word = line;
	        		indexOfDef = 0;
	        		defs = new String[AMOUNT_OF_DEFS];
	        	}
	        	else {        		
	        		defs[indexOfDef] = line.substring(1);
	        		indexOfDef++;
	        	}
	        	
	        }
	    } finally {
	        reader.close();
	    }
	}
	
	private long addWordToDict(String word,String[] defs){
		ContentValues initialValues = new ContentValues();
	    initialValues.put(COLUMN_WORD, word);
	    for(int i = 0; i<AMOUNT_OF_DEFS; i++){
	    	String def = (defs[i] != null)? defs[i] : "";
	    	initialValues.put(COLUMN_DEF+String.valueOf(i+1), def);
	    }
	    return mDatabase.insert(TABLE_FOR_DICT, null, initialValues);
	}
		
	private Cursor query(String selection, String[] selectionArgs, String[] columns) {
		Resources res = context.getResources();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);		
		String[] limits     = res.getStringArray(R.array.number_of_results);
		String defaultLimit = limits[limits.length-1];
		String limit        = prefs.getString(res.getString(R.string.number_of_result_desc), defaultLimit);

	    SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
	    builder.setTables(TABLE_FOR_DICT);
	    Cursor cursor = builder.query(getReadableDatabase(),
	    							  columns, selection, selectionArgs, null, null, null, limit);
	    Log.d(TAG,"Get cursor");
	    if (cursor == null) {
	    	Log.d(TAG,"Cursor is null");
	        return null;
	    } else {
	    	cursor.moveToFirst();
	    	return cursor;
	    }	    
	}
	
	public Cursor search(String query,int pos){
		while(!ready){
			Log.d(TAG,"waiting");
			// wait for db
		}
		if(query.equals("")){
			String selection3 = null;
	        String[] selectionArgs3 = null;
	        String[] columns3 = {"*","rowid as _id"};	
	        return query(selection3, selectionArgs3, columns3);	
		}
		switch(pos){
			case MainActivity.BOTH_POS:
				String selection3 =  TABLE_FOR_DICT + " MATCH ?";
		        String[] selectionArgs3 = new String[] {COLUMN_WORD+":*"+query+"* OR "+getStringForAllDefColumns(query)};
		        String[] columns3 = {"*","rowid as _id"};	
		        return query(selection3, selectionArgs3, columns3);					
			case MainActivity.WORD_POS:
				String selection1 = COLUMN_WORD + " MATCH ?";
		        String[] selectionArgs1 = new String[] {"*"+query+"*"};
		        String[] columns1 = {"*","rowid as _id"};	
		        return query(selection1, selectionArgs1, columns1);					
			default: //case MainActivity.DEF_POS:
				String selection2 = TABLE_FOR_DICT + " MATCH ?";
		        String[] selectionArgs2 = new String[] {getStringForAllDefColumns(query)};
		        String[] columns2 = {"*","rowid as _id"};	
		        return query(selection2, selectionArgs2, columns2);							
		}		
	}
	
	private String getStringForAllDefColumns(String query){
		String result = "";
		for(int i = 0; i<AMOUNT_OF_DEFS; i++){
	    	result += " " + COLUMN_DEF + String.valueOf(i+1) + ":*" + query + "* ";
	    	if(i+1 != AMOUNT_OF_DEFS){
	    		result += "OR";
	    	}
	    }
		return result;
	}
	
	/**
	 * Check if the database exist
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	public boolean ifDatabaseExist() {
	    File dbFile = context.getDatabasePath(DATABASE_NAME);
	    return dbFile.exists();
	}
}
