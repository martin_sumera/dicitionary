package com.sumera.slovnik.animations;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class ExpendListViewAnimation extends Animation {
	
	private View mAnimatedView;
	private LayoutParams mViewLayoutParams;
	private int mMarginStart, mMarginEnd;
	private boolean mIsVisibleAfter = false;
	private boolean mWasEndedAlready = false;
	
	/**
	 * Initialize the animation
	 * 
	 * @param view
	 *            The layout we want to animate
	 * @param duration
	 *            The duration of the animation, in ms
	 */
	public ExpendListViewAnimation(View view, int duration) {
		setDuration(duration);
		mAnimatedView = view;
		mViewLayoutParams = (LayoutParams) view.getLayoutParams();
		
		// Decide to show or hide the view
		if(view.getVisibility() == View.VISIBLE){
			mIsVisibleAfter = true;
		}
		else {
			Log.d("HEIGHT",String.valueOf(mViewLayoutParams.height));
			mViewLayoutParams.bottomMargin = 0 - mViewLayoutParams.height;
			mAnimatedView.requestLayout();
		}
		
		mMarginStart = mViewLayoutParams.bottomMargin;
		mMarginEnd = (mMarginStart == 0 ? (0 - view.getHeight()) : 0);
		view.setVisibility(View.VISIBLE);
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		super.applyTransformation(interpolatedTime, t);
		Log.d("ANIM",String.valueOf(mMarginStart + (int) ((mMarginEnd - mMarginStart) * interpolatedTime)));
		if (interpolatedTime < 1.0f) {
			// Calculating the new bottom margin, and setting it
			mViewLayoutParams.bottomMargin = mMarginStart + (int) ((mMarginEnd - mMarginStart) * interpolatedTime);
			// Invalidating the layout, making us seeing the changes we made
			mAnimatedView.requestLayout();
			// Making sure we didn't run the ending before (it happens!)
		}
		else if (!mWasEndedAlready) {
			mViewLayoutParams.bottomMargin = mMarginEnd;
			mAnimatedView.requestLayout();
			if (mIsVisibleAfter) {
				mAnimatedView.setVisibility(View.GONE);
			}
			mWasEndedAlready = true;
		}
	}
}
