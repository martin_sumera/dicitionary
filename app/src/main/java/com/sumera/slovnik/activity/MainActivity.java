package com.sumera.slovnik.activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.sumera.slovnik.R;
import com.sumera.slovnik.adapters.MyPagerAdapter;
import com.sumera.slovnik.data.DatabaseOpenHelper;
import com.sumera.slovnik.fragments.MyListViewFragment;

/**
 * MainActivity for search in dictionary
 * 
 * Contains result for dictionary search
 *  
 * @author Martin Sumera
 * @version 17.09.2014
 * @since 1.0
 */
public class MainActivity extends ActionBarActivity{
	
	public static final String TAG = "MainActivity";
	
	public static final int NUMBER_OF_SWIPE_VIEWS = 3;
	
	/*
	 * Fields for fragments
	 */
	private MyListViewFragment bothFragment,wordFragment,defFragment;
	
	public static final int BOTH_POS = 0, WORD_POS = 1, DEF_POS = 2;
	
	/*
	 * Fields for PagerFragment (tabs)
	 */
	private MyPagerAdapter mAdapter;
	private ViewPager	   mPager;
	
	/*
	 * Fields for Navigation Drawer
	 */
	private String[]              mDrawerMenu;
    private DrawerLayout          mDrawerLayout;
    private ListView              mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // Intent for UserSettingsActivity AuthorInfoActivity
    private Intent userSettingsActivity, authorInfoActivity;
    
    // SearchView displayed in action bar
    private SearchView searchView;
    
    // Resources
    private Resources resources;
    
    // DB connection
    private DatabaseOpenHelper mDbHelper;
    
    // Field for search query
    private String searchQuery = "";
    
    // Field for save searchQuery onSaveInstanceState()
    private static final String STATE_QUERY = "query";

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString(STATE_QUERY, searchQuery);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
	    
	    // Get resources
	    resources = getResources();
	    
	    // Set intent for UserSettingsActivity
	    userSettingsActivity = new Intent(this, com.sumera.slovnik.activity.UserSettingsActivity.class);
	 	
	    // Set intent for AuthorInfoActivity
	    authorInfoActivity = new Intent(this, com.sumera.slovnik.activity.AuthorInfoActivity.class);
	 	
	    
	    // Set PagerFragment (tabs) 
	    mAdapter = new MyPagerAdapter(getSupportFragmentManager(),this);    
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(2);
        mPager.setAdapter(mAdapter);
        mPager.getId();
        SimpleOnPageChangeListener pageListener = new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between pages, select the corresponding tab.
                getActionBar().setSelectedNavigationItem(position);
            }
        };
        mPager.setOnPageChangeListener(pageListener);
        	       
	    // Initialize navigation drawer on left side
	    initializeNavigationDrawer();
	    
	    // Initialize tabs navigation
	    initializeTabs();

	    initializeDatabase();  
	    
	    if (savedInstanceState != null) {    	
	        searchQuery = savedInstanceState.getString(STATE_QUERY);
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        
        // Get SearchView from action bar
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Expand and request action bar
        searchView.setIconified(false);
        // Set SearchView Listener
        setSearchViewListener(searchView);
        
        if(!searchQuery.equals("")){
        	searchView.setQuery(searchQuery,false);
        }

        return super.onCreateOptionsMenu(menu);
	}
	
	/**
     * Setup onQueryTextListener for action bar
     * 
     * @param searchView SearchView from action bar
     * @return void
     */
	private void setSearchViewListener(SearchView searchView){
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
		    @Override
		    public boolean onQueryTextChange(String newText) {
		    	performSearch(newText);
		        return true;
		    }

		    @Override
		    public boolean onQueryTextSubmit(String query) {
		    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		    	return true;
		    }
		});
	}
	
	/**
	 * Navigation Drawer implements OnItemClickListener
	 */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	String action = mDrawerMenu[position];
        	/*switch(action){
        		case "Nastavenia":
        			startActivity(userSettingsActivity);
        			break;
        		case "About":
        			startActivity(authorInfoActivity);
        	}*/
            switch(position){
        		case 0:
        			startActivity(userSettingsActivity);
        			break;
        		case 1:
        			startActivity(authorInfoActivity);
        	}
        	mDrawerLayout.closeDrawers(); 	
        }
    }
	
	/**
	 * Setup Navigation Drawer on left side
	 */
	private void initializeNavigationDrawer(){		
		mDrawerMenu	  = getResources().getStringArray(R.array.menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList   = (ListView) findViewById(R.id.left_drawer);
        
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, mDrawerMenu));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener()); 		
        // Set the drawer toggle listener
        mDrawerToggle = new ActionBarDrawerToggle(this,                  /* host Activity */
								                  mDrawerLayout,         /* DrawerLayout object */
								                  R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
								                  R.string.drawer_open,  /* "open drawer" description */
								                  R.string.drawer_close  /* "close drawer" description */
								                  ){   

            /** Called when a drawer closed */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer open. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true); 
	}
	
	private void initializeTabs(){
	    final ActionBar actionBar = getActionBar();
		
	    // Specify that tabs should be displayed in the action bar.
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

	    // Create a tab listener that is called when the user changes tabs.
	    ActionBar.TabListener tabListener = new ActionBar.TabListener() {
			@Override
			public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
				mPager.setCurrentItem(tab.getPosition());				
			}

			@Override
			public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {		
			}

			@Override
			public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {				
			}
	    };

	    // Tab for search result
	    actionBar.addTab(actionBar.newTab()
                        		  .setText(resources.getString(R.string.tab_search_both))
                                  .setTabListener(tabListener));
	    
	    // Tab for similar words
	    actionBar.addTab(actionBar.newTab()
      		  					  .setText(resources.getString(R.string.tab_search_word))
      		  					  .setTabListener(tabListener));
	    
	    // Tab for similar words
	    actionBar.addTab(actionBar.newTab()
      		  					  .setText(resources.getString(R.string.tab_search_def))
      		  					  .setTabListener(tabListener));
		
	}
	
	private void initializeDatabase(){
		// Get database
	    mDbHelper = new DatabaseOpenHelper(this); 
	    
	    if(!mDbHelper.ifDatabaseExist()){
	    	new AsyncTask<Void, Void, Void>(){
	            @Override
	            protected Void doInBackground(Void... params) {
	            	mDbHelper.getWritableDatabase();
	                return null;
	            }
				@Override
				protected void onPostExecute(Void result) {
					databaseIsReady();
					super.onPostExecute(result);
				}
	            
	        }.execute();
	    }
	    else {
	    	databaseIsReady();
	    	Log.d(TAG,"Database exist");
	    }
	    mDbHelper.close();
	}
	
	public void getFragments(){
		if(bothFragment == null){
    		bothFragment = (MyListViewFragment)findFragmentByPosition(BOTH_POS);
    	}
    	if(wordFragment == null){
    		wordFragment = (MyListViewFragment)findFragmentByPosition(WORD_POS);
    	}
    	if(defFragment == null){
    		mAdapter.getItem(2);
    		defFragment = (MyListViewFragment)findFragmentByPosition(DEF_POS);
    	}
	}
	
	public Fragment findFragmentByPosition(int position) {
	    return getSupportFragmentManager().findFragmentByTag(
	            "android:switcher:" + mPager.getId() + ":"
	                    + mAdapter.getItemId(position));
	}
	
	private void databaseIsReady(){
		mDbHelper = new DatabaseOpenHelper(this);  
		mDbHelper.getWritableDatabase();		
	}
	
	private void performSearch(String query){
		getFragments();
		searchQuery = query;
    	bothFragment.search(query);
    	wordFragment.search(query);
    	defFragment.search(query);
	}
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
}
