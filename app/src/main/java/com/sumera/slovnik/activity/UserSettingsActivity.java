package com.sumera.slovnik.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.sumera.slovnik.fragments.MyPreferencesFragment;

public class UserSettingsActivity extends PreferenceActivity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferencesFragment()).commit();
    }

}
