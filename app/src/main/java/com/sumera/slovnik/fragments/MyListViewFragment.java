package com.sumera.slovnik.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sumera.slovnik.R;
import com.sumera.slovnik.adapters.ListViewCursorAdapter;
import com.sumera.slovnik.data.DatabaseOpenHelper;

public class MyListViewFragment extends Fragment implements OnItemClickListener {

	public static final String TAG = "MyListViewFragment";

	private int fragmentPosition;

	DatabaseOpenHelper mDbHelper;

	ArrayAdapter<String> adapter;

	Context context;

	public TextView text;
	
	/**
	 * Initialize new MyListViewFragment class
	 * 
	 * @param position
	 *            Position in pager adapter
	 * @return
	 */
	public static MyListViewFragment init(int position) {
		MyListViewFragment fragment = new MyListViewFragment();

		// Initialize new MyListViewFragment.
		Bundle args = new Bundle();
		args.putInt("position", position);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fragmentPosition = getArguments() != null ? getArguments().getInt("position") : 0;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layoutView = inflater.inflate(R.layout.list_view_fragment_dictionary, container, false);;

		return layoutView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		ListView lv = (ListView) getView().findViewById(R.id.listView);
		
		lv.setOnItemClickListener(new OnItemClickListener()
		{
		    @Override 
		    public void onItemClick(AdapterView<?> arg0, View view,int position, long arg3)
		    { 
		    	
		    	View toolbar = view.findViewById(R.id.toolbar);
		    	Log.d(TAG,"onClick" + String.valueOf(toolbar.getVisibility()));
		    	if(toolbar.getVisibility() == View.VISIBLE){
		    		toolbar.setVisibility(View.GONE);
		    	}
		    	else {
		    		toolbar.setVisibility(View.VISIBLE);
		    	}

				// Creating the expand animation for the item
				//ExpendListViewAnimation expandAni = new ExpendListViewAnimation(toolbar, 500);
				// Start the animation on the toolbar
				//view.startAnimation(expandAni);		    
		    }
		});
		
		
		search("");
	}

	public void search(String query) {
		showListView(false);
	
		LoadFromDb async = new LoadFromDb();
		async.execute(query);		
	}

	private class LoadFromDb extends AsyncTask<String, Void, Cursor> {	
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			String[] columns = new String[DatabaseOpenHelper.AMOUNT_OF_DEFS + 1];
			columns[0] = DatabaseOpenHelper.COLUMN_WORD;
			for (int i = 0; i < DatabaseOpenHelper.AMOUNT_OF_DEFS; i++) {
				columns[i + 1] = DatabaseOpenHelper.COLUMN_DEF + String.valueOf(i + 1);
			}
		}

		@Override
		protected Cursor doInBackground(String... params) {	
			if (mDbHelper == null)
				initDatabase();		
			return mDbHelper.search(params[0],fragmentPosition);
		}

		@Override
		protected void onPostExecute(Cursor result) {
			super.onPostExecute(result);
			ListViewCursorAdapter adapter = new ListViewCursorAdapter(context,result, true);
			ListView listView = (ListView) getView().findViewById(R.id.listView);
			listView.setAdapter(adapter);
			showListView(true);
		}	

	}
	
	public void showListView(boolean show){
		View view = getView();
		if(show){
			ListView lv = (ListView) view.findViewById(R.id.listView);
			lv.setVisibility(View.VISIBLE);
			ProgressBar pb = (ProgressBar) view.findViewById(R.id.progressBar);
			pb.setVisibility(View.GONE);
		}
		else {
			ListView lv = (ListView) view.findViewById(R.id.listView);
			lv.setVisibility(View.GONE);
			ProgressBar pb = (ProgressBar) view.findViewById(R.id.progressBar);
			pb.setVisibility(View.VISIBLE);
		}
		
	}

	private void initDatabase() {
		mDbHelper = new DatabaseOpenHelper(context);
		mDbHelper.getReadableDatabase();
	}

	public int getPosition() {
		return fragmentPosition;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Log.d(TAG,"click");
		

	}

}
