package com.sumera.slovnik.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sumera.slovnik.R;
import com.sumera.slovnik.data.DatabaseOpenHelper;

public class ListViewCursorAdapter extends CursorAdapter {
	
	public static final String TAG = "ListViewCursorAdapter";

	LayoutInflater mInflater;
	
	public ListViewCursorAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getItemViewType(int position) {
	    return 0;
	}

	@Override
	public int getViewTypeCount() {
	    return 1;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {		
		TextView title = (TextView) view.findViewById(R.id.title);
	    title.setText(cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_WORD)));
		
		for(int i=0;i<DatabaseOpenHelper.AMOUNT_OF_DEFS;i++){
	    	String def = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_DEF+String.valueOf(i+1)));

	    	TextView defTv;
	    	switch(i+1){
		    	case 1:
		    		defTv  = (TextView) view.findViewById(R.id.desc1);
		    		break;
		    	case 2:
		    		defTv  = (TextView) view.findViewById(R.id.desc2);
		    		break;
		    	case 3:
		    		defTv  = (TextView) view.findViewById(R.id.desc3);
		    		break;
		    	default:
		    		defTv  = (TextView) view.findViewById(R.id.desc4);
		    		break;
	    	}
	    	defTv.setText(def);
	    	
	    	if(def.equals("")){
	    		defTv.setVisibility(View.GONE);
	    	}
	    	else {
	    		defTv.setVisibility(View.VISIBLE);
	    	}
		}
		
		Resources res = context.getResources();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);		
		boolean showDesc         = prefs.getBoolean(res.getString(R.string.show_definitions), false);

		LinearLayout ll = (LinearLayout) view.findViewById(R.id.toolbar);
		if(showDesc){
			ll.setVisibility(View.VISIBLE);
		}
		else {
			ll.setVisibility(View.GONE);
		}
		
		
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		Log.d(TAG,"newView()");
	    //sViewHolder holder = new ViewHolder();
	    View view = null;
	    
	    view = mInflater.inflate(R.layout.dictionary_one_item_list_view, parent, false);
	    	    
	    for(int i=0;i<DatabaseOpenHelper.AMOUNT_OF_DEFS;i++){
	    	String def = cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.COLUMN_DEF+String.valueOf(i+1)));

	    	TextView defTv;
	    	switch(i+1){
		    	case 1:
		    		defTv  = (TextView) view.findViewById(R.id.desc1);
		    		break;
		    	case 2:
		    		defTv  = (TextView) view.findViewById(R.id.desc2);
		    		break;
		    	case 3:
		    		defTv  = (TextView) view.findViewById(R.id.desc3);
		    		break;
		    	default:
		    		defTv  = (TextView) view.findViewById(R.id.desc4);
		    		break;
	    	}
	    	//defTv.setText(def);
	    	
	    	if(def.equals("")){
	    		defTv.setVisibility(View.GONE);
	    	}
	    	
	    }
	    	
	    return view;
	}
	
	

}
