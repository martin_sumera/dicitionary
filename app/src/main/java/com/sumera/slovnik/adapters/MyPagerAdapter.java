package com.sumera.slovnik.adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.sumera.slovnik.activity.MainActivity;
import com.sumera.slovnik.fragments.MyListViewFragment;

public class MyPagerAdapter extends FragmentPagerAdapter {
	
	public static final String TAG = "MyPagerAdapter";

	public MyPagerAdapter(FragmentManager fragmentManager,Context context) {	
		super(fragmentManager);
	}

	@Override
	public int getCount() {
		return MainActivity.NUMBER_OF_SWIPE_VIEWS;
	}

	@Override
	public MyListViewFragment getItem(int position) {
		return MyListViewFragment.init(position);
	}
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
		MyListViewFragment fragment = (MyListViewFragment) super.instantiateItem(container, position);
        
        return fragment;
    }
	
}
